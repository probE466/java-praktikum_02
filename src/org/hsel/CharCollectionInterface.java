package org.hsel;

/**
 * Created by fred on 08.04.16.
 */
public interface CharCollectionInterface {

    int size();

    int count(char c);

    int different();

    char top();

    String toString();

    CharCollection moreThan(int m);

    boolean equals(Object x);

    CharCollection except(CharCollection cc);
}

