package org.hsel;

import org.junit.Test;

import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;


public class CharCollectionTest {

    @Test
    public void it_can_initialize() {
        CharCollection c = new CharCollection('A', 'B', 'R', 'A', 'K', 'A', 'D', 'A', 'B', 'R', 'A');

        assertEquals("(A, B, R, A, K, A, D, A, B, R, A)", c.toString());
    }

    @Test(expected = InputMismatchException.class)
    public void it_throws_an_exception_when_illegal_char_number() {
        CharCollection a = new CharCollection('2');
    }

    @Test(expected = InputMismatchException.class)
    public void it_throws_an_exception_when_illegal_char_special_sign() {
        CharCollection b = new CharCollection('&');
    }

    @Test(expected = InputMismatchException.class)
    public void it_throws_an_exception_when_illegal_char_downcase() {
        CharCollection c = new CharCollection('a');
    }

    @Test(expected = InputMismatchException.class)
    public void it_throws_an_exception_when_illegal_char_dot() {
        CharCollection x = new CharCollection('.');
    }

    @Test
    public void it_can_initialize_string() {
        CharCollection c = new CharCollection("ABRAKADABRA");

        assertEquals("(A, B, R, A, K, A, D, A, B, R, A)", c.toString());
    }

    @Test
    public void it_can_max() {
        CharCollection c = new CharCollection("ABRAKADABRA");
        assertEquals('A', c.topVersionB());
    }

    @Test
    public void it_can_max_with_one_char() {
        CharCollection c = new CharCollection("A");
        assertEquals('A', c.topVersionB());
    }

    @org.junit.Test
    public void size() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");
        assertEquals(11, c.size());
    }

    @org.junit.Test
    public void count() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");
        assertEquals(2, c.count('R'));
    }

    @org.junit.Test
    public void different() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");
        assertEquals(5, c.different());
    }

    @org.junit.Test
    public void top() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");

        assertEquals('A', c.top());
    }

    @org.junit.Test
    public void moreThan() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");
        assertEquals("(A, B, R, A, A, A, B, R, A)", c.moreThan(1).toString());

    }

    @Test
    public void equals() {
        CharCollection c = new CharCollection("ABRAKADABRA");
        CharCollection x = new CharCollection("AKADABRAABR");

        assertEquals(true, c.equals(x));
    }

    @Test
    public void equalsFalse() {
        CharCollection c = new CharCollection("FREED");
        CharCollection x = new CharCollection("FROED");

        assertEquals(false, c.equals(x));
    }

    @Test
    public void equals_different_count_of_chars() {
        CharCollection c = new CharCollection("FREED");
        CharCollection x = new CharCollection("FRRED");

        assertEquals(false, c.equals(x));
    }

    @Test
    public void equals_different_length() {
        CharCollection c = new CharCollection("FREED");
        CharCollection x = new CharCollection("FRED");

        assertEquals(false, c.equals(x));
    }

    @Test
    public void equals_different_object() {
        CharCollection c = new CharCollection("FREED");
        Integer x = 12;

        assertEquals(false, c.equals(x));
    }

    @org.junit.Test
    public void except() throws Exception {
        CharCollection c = new CharCollection("ABRAKADABRA");
        CharCollection x = new CharCollection("ABR");

        assertEquals("(K, D)", c.except(x).toString());
    }
}