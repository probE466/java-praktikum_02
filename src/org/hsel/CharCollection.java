package org.hsel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

public class CharCollection implements CharCollectionInterface {

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    private ArrayList<Character> characterList = new ArrayList<>();

    public CharCollection(char... cc) {

        for (char c : cc) {
            if (!Character.toString(c).matches("([A-Z])")) {
                throw new InputMismatchException();
            }
            characterList.add(c);
        }

    }

    public CharCollection(String s) {
        this(s.toCharArray());
    }

    private CharCollection(ArrayList<Character> a) {
        characterList = a;
    }

    @Override
    public String toString() {
        StringBuilder st = new StringBuilder();
        st.append('(');
        for (int i = 0; i < characterList.size(); i++) {
            char tmp = characterList.get(i);
            st.append(tmp);
            if (characterList.size() - 1 != i) {
                st.append(',');
                st.append(" ");
            }
        }
        st.append(')');
        return st.toString();
    }

    @Override
    public int size() {
        return characterList.size();
    }

    @Override
    public int count(char c) {
        int tmpCount = 0;
        for (Character aCharacterList : characterList) {
            if (aCharacterList == c) {
                tmpCount++;
            }
        }
        return tmpCount;
    }

    @Override
    public int different() {
        int tmpCount = 0;
        ArrayList<Character> isUsed = new ArrayList<>();
        for (Character aCharacterList : characterList) {
            if (!isUsed.contains(aCharacterList)) {
                tmpCount++;
                isUsed.add(aCharacterList);
            }
        }
        return tmpCount;
    }


    public char topVersionB() {
        if (characterList.size() == 0) {
            return 0;
        }

        Character top = null;
        int countTop = 0;
        Map<Character, Integer> map = toHashMap(this);

        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() > countTop) {
                countTop = entry.getValue();
                top = entry.getKey();
            }
        }

        return top;

    }

    @Override
    public char top() {

        if (this.getCharacterList().size() == 0) {
            return 0;
        }

        int currMax = 0;
        char currMaxChar = '0';
        int tmpCount;

        for (Character tmpChar : characterList) {
            tmpCount = getCount(tmpChar);
            if (tmpCount > currMax) {
                currMax = tmpCount;
                currMaxChar = tmpChar;
            }
        }
        return currMaxChar;
    }

    private int getCount(char c) {
        HashMap<Character, Integer> map = toHashMap(this);
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getKey() == c) {
                return entry.getValue();
            }
        }
        return 0;
    }

    private HashMap<Character, Integer> toHashMap(CharCollection a) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (Character aCharacterList : a.getCharacterList()) {
            char c = aCharacterList;

            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        return map;
    }

    @Override
    public boolean equals(Object x) {

        if (x == null) {
            return false;
        }
        if (x.getClass() != CharCollection.class) {
            return false;
        }
        CharCollection a = (CharCollection) x;
        if (this.getCharacterList().size() != a.getCharacterList().size()) {
            return false;
        }
        HashMap<Character, Integer> mapA = toHashMap(this);
        HashMap<Character, Integer> mapB = toHashMap(a);


        for (Map.Entry<Character, Integer> entry : mapA.entrySet()) {
            if (mapB.containsKey(entry.getKey()) && !entry.getValue().equals(mapB.get(entry.getKey()))) {
                return false;
            }
        }

        for (Map.Entry<Character, Integer> entry : mapB.entrySet()) {
            if (mapA.containsKey(entry.getKey()) && !entry.getValue().equals(mapB.get(entry.getKey()))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public CharCollection moreThan(int m) {
        ArrayList<Character> returnList = new ArrayList<>();
        for (int i = 0; i < this.characterList.size(); i++) {
            if (getCount(this.getCharacterList().get(i)) > m) {
                returnList.add(this.characterList.get(i));
            }
        }

        return new CharCollection(returnList);
    }

    @Override
    public CharCollection except(CharCollection cc) {
        ArrayList<Character> returnList = new ArrayList<>();
        Map<Character, Integer> deleteMap = cc.toHashMap(cc);
        boolean deleteMapContains;

        for (Character character : characterList) {
            deleteMapContains = deleteMap.containsKey(character);

            if (!deleteMapContains) {
                returnList.add(character);
            }

            if (deleteMapContains && !(deleteMap.get(character) > 0)) {
                returnList.add(character);
            }

            if (deleteMapContains && deleteMap.get(character) > 0) {
                deleteMap.replace(character, deleteMap.get(character) - 1);
            }

        }

        return new CharCollection(returnList);
    }
}
